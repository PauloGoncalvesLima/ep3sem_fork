class Produto < ApplicationRecord
  before_destroy :not_refereced_by_any_line_item
  belongs_to :user, optional: true
  
  has_many :line_items

  mount_uploader :image, ImageUploader
  serialize :image, JSON 
  

  validates :Nome, :Descrição, :Preço, :Peso, :Quantidade, presence: true
  validates :Descrição, length: { maximum: 1000, too_long: "%{count} characters is the maximum aloud. "}
  validates :Nome, length: { maximum: 140, too_long: "%{count} characters is the maximum aloud. "}
  validates :Preço, length: { maximum: 7 }, numericality: {greater_than: 0, message: "O preço tem que ser maior que 0."}
  validates :Peso, length: { maximum: 7 }, numericality: {greater_than: 0, message: "O peso tem que ser maior que 0."}
  validates :Quantidade, length: { maximum: 7 }, numericality: {greater_than_or_equal_to: 1, message: "A quantidade tem que ser maior que ou igual a 1."}
  validates :image, presence: true
  
  
  private
  def not_refereced_by_any_line_item
    unless line_items.empty?
      errors.add(:base, "Line Item presente")
      throw :abort
    end
  end

end
